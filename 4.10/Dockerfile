FROM ubuntu:focal

RUN useradd -r pandacoin \
  && apt-get update -y \
  && apt-get install -y curl gnupg gosu \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ARG TARGETPLATFORM
ENV PANDACOIN_RELEASE=v4.10.5
ENV PANDACOIN_VERSION=4.10.5
ENV PANDACOIN_DATA=/home/pandacoin/.pandacoin
ENV PATH=/opt/pandacoin-${PANDACOIN_VERSION}/bin:$PATH

RUN set -ex \
  && if [ "${TARGETPLATFORM}" = "linux/amd64" ]; then export TARGETPLATFORM=x86_64-linux-gnu; fi \
  && if [ "${TARGETPLATFORM}" = "linux/arm64" ]; then export TARGETPLATFORM=aarch64-linux-gnu; fi \
  && if [ "${TARGETPLATFORM}" = "linux/arm/v7" ]; then export TARGETPLATFORM=arm-linux-gnueabihf; fi \
  && if [ "${TARGETPLATFORM}" = "linux/riscv64" ]; then export TARGETPLATFORM=riscv64-linux-gnu; fi \
  && gpg --batch --keyserver keyserver.ubuntu.com --recv-keys 8182C60B6CE61B2EC47F63C72EBDD67BAC8F08F1 \
  && curl -SLO https://gitlab.com/api/v4/projects/25484413/packages/generic/pandacoin/pandacoin-${PANDACOIN_RELEASE}/pandacoin-${PANDACOIN_VERSION}-${TARGETPLATFORM}.tar.gz \
  && curl -SLO https://gitlab.com/api/v4/projects/25484413/packages/generic/pandacoin/pandacoin-${PANDACOIN_RELEASE}/SHA256SUMS.asc \
  && gpg --verify SHA256SUMS.asc \
  && grep " pandacoin-${PANDACOIN_VERSION}-${TARGETPLATFORM}.tar.gz" SHA256SUMS.asc | sha256sum -c - \
  && tar -xzf *.tar.gz -C /opt \
  && rm *.tar.gz *.asc \
  && rm -rf /opt/pandacoin-${PANDACOIN_VERSION}/bin/pandacoin-qt

COPY docker-entrypoint.sh /entrypoint.sh

VOLUME ["/home/pandacoin/.pandacoin"]

EXPOSE 22444 22445 33444 33445 44444 44445

ENTRYPOINT ["/entrypoint.sh"]

CMD ["pandacoind"]